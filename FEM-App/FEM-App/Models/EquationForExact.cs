﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FEM_App.Models
{
    class EquationForExact
    {
        public double A11 { get; set; }
        public double D { get; set; }
        public RightPart F { get; set; }
    }
}
