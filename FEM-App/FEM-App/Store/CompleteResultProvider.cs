﻿using System;
using System.Collections.Generic;
using TriangleNet.Geometry;
using Newtonsoft.Json;

namespace FEM_App
{
	public static class CompleteResultProvider
	{
		private const string foldername = "../../Store/RESULT/";

		public static void Call(List<List<float>> ct, List<double> result, long timestamp)
		{
			var temp = new { CT = ct, Result = result };
			string output = JsonConvert.SerializeObject(temp, Formatting.Indented);
			string path = foldername + "result.json";
			System.IO.File.WriteAllText(path, output);
		}
	}
}

