﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FEM_App.Models
{
    class BoundaryForExact
    {
        public double U_A { get; set; }
        public double U_B { get; set; }
    }
}
