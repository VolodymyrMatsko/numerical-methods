﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FEM_App.Models
{
    public static class MockData
    {
        public static List<List<float>> GetCT()
        {
            return new List<List<float>>()
            {
                new List<float>(){0,0},
                new List<float>(){0,1},
                new List<float>(){0,2},
                new List<float>(){0,3},
                new List<float>(){1,0},
                new List<float>(){1,1},
                new List<float>(){1,2},
                new List<float>(){1,3}
            };
        }

        public static List<List<int>> GetNT()
        {
            return new List<List<int>>()
            {
                new List<int>(){0,4,1},
                new List<int>(){1,4,5},
                new List<int>(){1,5,2},
                new List<int>(){2,5,6},
                new List<int>(){3,2,6},
                new List<int>(){3,6,7}
            };
        }

        public static List<PointF> GetArea1()
        {
            List<PointF> contuor = new List<PointF>();
            contuor.Add(new PointF(0.0f, 0.0f));
            contuor.Add(new PointF(1.0f, 0.0f));
            contuor.Add(new PointF(0.0f, 1.0f));
            return contuor;
        }

        public static List<PointF> GetArea2()
        {
            List<PointF> contuor = new List<PointF>();
            contuor.Add(new PointF(0.0f, 0.0f));
            contuor.Add(new PointF(1.0f, 0.0f));
            contuor.Add(new PointF(1.0f, 2.0f));
            contuor.Add(new PointF(0.0f, 2.0f));
            return contuor;
        }

        public static List<PointF> GetArea3()
        {
            List<PointF> contuor = new List<PointF>();
            contuor.Add(new PointF(1.0f, 0.0f));
            contuor.Add(new PointF(2.0f, 0.0f));
            contuor.Add(new PointF(0.0f, 2.0f));
            contuor.Add(new PointF(0.0f, 1.0f));
            return contuor;
        }

        public static List<PointF> GetArea4()
        {
            List<PointF> contuor = new List<PointF>();
            contuor.Add(new PointF(-3.0f, 0.0f));
            contuor.Add(new PointF(3.0f, 0.0f));
            contuor.Add(new PointF(0.0f, 3.0f));
            return contuor;
        }

        public static List<PointF> GetArea5()
        {
            List<PointF> contuor = new List<PointF>();
            contuor.Add(new PointF(-2.0f, 0.0f));
            contuor.Add(new PointF(2.0f, 0.0f));
            contuor.Add(new PointF(2.0f, 4.0f));
            contuor.Add(new PointF(-2.0f, 4.0f));
            return contuor;
        }

        public static List<PointF> GetArea6()
        {
            List<PointF> contuor = new List<PointF>();
            contuor.Add(new PointF(-3.0f, 0.0f));
            contuor.Add(new PointF(-1.0f, 0.0f));
            contuor.Add(new PointF(0.0f, 1.0f));
            contuor.Add(new PointF(1.0f, 0.0f));
            contuor.Add(new PointF(3.0f, 0.0f));
            contuor.Add(new PointF(0.0f, 3.0f));
            return contuor;
        }

    }
}
