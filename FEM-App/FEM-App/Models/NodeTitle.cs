﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FEM_App.Models
{
    public class NodeTitle
    {
        public Label Label { get; }
        public NodeTitle(string text, PointF location)
        {
            Label = new Label();
            Label.Text = text;
            Label.Location = new Point((int)location.X, (int)location.Y);
            Label.Height = 12;
            Label.Width = 20;
            Label.BackColor = Color.LightBlue;
        }
    }
}
