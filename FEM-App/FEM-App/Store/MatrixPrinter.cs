﻿using System;
using System.Collections.Generic;
using TriangleNet.Geometry;

namespace FEM_App
{
	public static class MatrixPrinter
	{
		private const string foldername = "../../Store/Matrixes/";
        private const string foldernameRes = "../../Store/Result/";

        public static void Call(List<List<double>> matrix, string identity, string matrix_type, long timestamp)
		{
			    System.IO.Directory.CreateDirectory ($"{foldername}/{matrix_type}-{timestamp}");

			using (System.IO.StreamWriter file 
				= new System.IO.StreamWriter (GetFileName (foldername, identity, matrix_type, timestamp,false))) {
				foreach (var row in matrix) {
					foreach (var column in row) {
						file.Write (String.Format ("{0:F10} ", column));						
					}
					file.WriteLine ();
				}
			}
		}

        public static void CallResult(List<double> arr, List<List<float>> ct, string identity, string matrix_type, long timestamp)
        {


            //folder = isRes ? foldernameRes : foldername;
            //if (!isRes)
            //    System.IO.Directory.CreateDirectory($"{folder}/{matrix_type}-{timestamp}");

            using (System.IO.StreamWriter file
                = new System.IO.StreamWriter(GetFileName(foldernameRes, identity, matrix_type, timestamp, true)))
            {
                for (int i = 0; i < arr.Count; ++i)
                {
                    file.WriteLine(String.Format("{0} {1} {2:F10} ", ct[i][0], ct[i][1], arr[i]));
                }

                file.WriteLine();
            }
        }

        private static string GetFileName(string folder,string nt, string matrix_type, long timestamp,bool isRes)
		{
			return !isRes ? $"{folder}/{matrix_type}-{timestamp}/matrix({nt}).txt" : $"{folder}/{matrix_type}.txt";

        }
	}
}

