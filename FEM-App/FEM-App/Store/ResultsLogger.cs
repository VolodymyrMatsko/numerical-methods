﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using TriangleNet.Geometry;

namespace FEM_App.Store
{
    public static class ResultsLogger
    {
		private const string foldername = "../../Store/Log/";
		
        public static void Call(List<List<float>> ct, 
                                List<List<int>> nt,
                                List<List<Edge>> ntg)
        {
            var temp = new { CT = ct, NT = nt, NTG = ntg };
            string output = JsonConvert.SerializeObject(temp, Formatting.Indented);
            string path = foldername + GetFileName();
            System.IO.File.WriteAllText(path, output);
        }

        private static string GetFileName()
        {
            return $"{DateTime.Now.Ticks}-results.json";
        }
    }
}
