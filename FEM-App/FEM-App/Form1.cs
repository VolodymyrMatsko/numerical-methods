﻿using FEM_App.Models;
using FEM_App.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TriangleNet.Geometry;
using FEM_App.Store;
using FEM_App.Store.Data;
using System.IO;
using FEM_App.Heplers;

namespace FEM_App
{
    public partial class Form1 : Form
    {
        delegate List<PointF> GetArea();

        private DrawingServices _drawingService;
        private List<MeshService> _meshServices;
        private MatrixService _matrixService;
        private bool drawNodeTitle = false;
        private bool drawElementTitle = false;
        private int _idArea = 0;
        private int _scaleIndex = 90;
        private bool _ifareain_x = true;
        private double r = 1.01;
        private double Line = 1.0;
        private DrawGraphAndErrors _formForDrawGraph;
        private List<ResultVectorWithPoint> _resultOnLine;
        private List<ResultVectorWithPoint> _resultExact;

        public Form1()
        {
            InitializeComponent();

            _drawingService = new DrawingServices(panel1);
            _meshServices = new List<MeshService>();

            for (int i = 0; i < 2; i++)
            {
                _meshServices.Add(null);
            }
        }

        private void restartCreateProgect(double maxArea, GetArea area)
        {
            maxArea *= 0.48;
            _drawingService.ClearePanel();
            var areas = new List<GetArea>()
                        {
                            () => AreasProvider.GetArea(1),
                            () => AreasProvider.GetArea(2)
                        };
            var pens = new List<Pen>()
                        {
                            new Pen(Brushes.Red, 2),
                            new Pen(Brushes.Green, 2)
                        };

            for (int i = 0; i < 2; i++)
            {
                _meshServices[i] = new MeshService(areas[i](), maxArea);
                _meshServices[i].Triangulate();

                _drawingService._CT = _meshServices[i].CT;
                _drawingService._NT = _meshServices[i].NT;
                _drawingService.scaleIndex = _scaleIndex;
                _drawingService.ifareain_x = _ifareain_x;
                Draw(pens[i]);
                if (elementsTitle.Checked)
                {
                    elementsTitle.Checked = false;
                }
                if (nodesTitle.Checked)
                {
                    nodesTitle.Checked = false;
                }
            }
            ClearRichTextBoxs();
            WriteArrays(0);
            WriteArrays(1);
        }

        private void btDraw_Click(object sender, EventArgs e)
        {
            DrawPointBelongInAnotherArea(0, 1);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DrawPointBelongInAnotherArea(1, 0);
        }

        public void DrawPointBelongInAnotherArea(int indexAreaWithPoint, int indexAreaFindTriangle)
        {
            _drawingService.ClearePanel();

            var CT_AreaFindTriangle = _meshServices[indexAreaFindTriangle].CT;
            var NT_AreaFindTriangle = _meshServices[indexAreaFindTriangle].NT;

            var CT_AreaWithPoints = _meshServices[indexAreaWithPoint].CT;
            var NT_AreaWithPoints = _meshServices[indexAreaWithPoint].NT;

            _drawingService._CT = CT_AreaFindTriangle;
            _drawingService._NT = NT_AreaFindTriangle;
            _drawingService.scaleIndex = _scaleIndex;
            _drawingService.ifareain_x = _ifareain_x;
            Draw(new Pen(Brushes.Green, 1));

            var allTriangleAreaFindTriangle = Helper.ToTriangleList_NT(CT_AreaFindTriangle, NT_AreaFindTriangle);
            var pointInJointAreaWithPoints = PointHelper.GetPointInArea(Helper.ToPointFListCT(CT_AreaWithPoints), AreasProvider.GetArea(0));

            _drawingService._CT = CT_AreaWithPoints;
            _drawingService._NT = NT_AreaWithPoints;
            _drawingService.scaleIndex = _scaleIndex;
            _drawingService.ifareain_x = _ifareain_x;
            Draw(new Pen(Brushes.Red, 2));

            var triangleWithBelongPoint = PointHelper.GetTriangleWithBelongPoint(pointInJointAreaWithPoints, allTriangleAreaFindTriangle);
            _drawingService.DrawTriangles(new Pen(Brushes.Black, 2), triangleWithBelongPoint);
            _drawingService.DrawPoints(new Pen(Color.Black), pointInJointAreaWithPoints);
        }

        private void ClearRichTextBoxs()
        {
            richTextBox1.Text = "";
            richTextBox2.Text = "";
            richTextBox3.Text = "";
        }

        private void WriteArrays(int index)
        {
            richTextBox1.Text += $"{index+1} area\n";
            richTextBox2.Text += $"{index+1} area\n";
            richTextBox3.Text += $"{index+1} area\n";

            //ResultsLogger.Call(_meshServices.CT, _meshServices.NT, _meshServices.NTG);

            for (int i = 0; i < _meshServices[index].CT.Count; i++)
            {
                richTextBox1.Text += string.Format("{0,3}| {1:n3} {2:n3}\n", i, _meshServices[index].CT[i][0],
                    _meshServices[index].CT[i][1]);
            }

            for (int i = 0; i < _meshServices[index].NT.Count; i++)
            {
                richTextBox2.Text += string.Format("{0,3}| {1,3} {2,3} {3,3}\n", i, _meshServices[index].NT[i][0],
                    _meshServices[index].NT[i][1], _meshServices[index].NT[i][2]);
            }

            for (int i = 0; i < _meshServices[index].NTG.Count; i++)
            {
                richTextBox3.Text += "Bounde " + (i + 1) + "\n";
                for (int j = 0; j < _meshServices[index].NTG[i].Count; j++)
                {
                    richTextBox3.Text += string.Format("{0,3}| {1,3} {2,3}\n", i, _meshServices[index].NTG[i][j].P0,
                        _meshServices[index].NTG[i][j].P1);
                }
            }
        }

        private void addLabelBtn_Click(object sender, EventArgs e)
        {
            //Draw();
        }

        private void Draw(Pen drawPen)
        {
            _drawingService.DrawTriangles(drawElementTitle, drawPen);
            if (drawNodeTitle)
            {
                _drawingService.DrawTitles();
            }
        }

        private void elementsTitle_CheckedChanged(object sender, EventArgs e)
        {
            drawElementTitle = !drawElementTitle;
            //panel1.CreateGraphics().Clear(Color.White);
            panel1.Controls.Clear();
            //Draw();
        }

        private void nodesTitle_CheckedChanged(object sender, EventArgs e)
        {
            drawNodeTitle = !drawNodeTitle;
            //panel1.CreateGraphics().Clear(Color.White);
            panel1.Controls.Clear();
            //Draw();
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            r = ((double) trackBar1.Maximum - (double) trackBar1.Value) / (double) trackBar1.Maximum + 0.01;
            //r = 0.11;
            //richTextBox1.Text += (r + "\n");
            textBox1.Text = r.ToString();
            restartCreateProgect(r, () => AreasProvider.GetArea(_idArea));
        }

        private void comboBox1_SelectedValueChanged(object sender, EventArgs e)
        {
            //switch ((string)comboBox1.SelectedItem)
            //{
            //    case "Area 1":
            //        _idArea = 0;
            //        _scaleIndex = 400;
            //        _ifareain_x = true;
            //        break;
            //    case "Area 2":
            //        _ifareain_x = true;
            //        _idArea = 1;
            //        _scaleIndex = 250;
            //        break;
            //    case "Area 3":
            //        _scaleIndex = 250;
            //        _ifareain_x = true;
            //        _idArea = 2;
            //        break;
            //    case "Area 4":
            //        _scaleIndex = 100;
            //        _ifareain_x = false;
            //        _idArea = 3;
            //        break;
            //    case "Area 5":
            //        _scaleIndex = 100;
            //        _ifareain_x = false;
            //        _idArea = 4;
            //        break;
            //    case "Area 6":
            //        _scaleIndex = 150;
            //        _ifareain_x = true;
            //        _idArea = 5;
            //        break;
            //}
            //restartCreateProgect(r, () => AreasProvider.GetArea(_idArea));
        }

        private void calculateBtn_Click(object sender, EventArgs e)
        {
            restartCreateProgect(r, () => AreasProvider.GetArea(_idArea));
        }

        private void CleanAllFolders()
        {
            var directory = "../../Store/Matrixes/";
            var di = new DirectoryInfo(directory);
            foreach (var file in di.GetFiles())
            {
                file.Delete();
            }

            foreach (var dir in di.GetDirectories())
            {
                dir.Delete(true);
            }
        }

        private void FillAnalysis(List<double> steps, double l2order, double w2order)
        {
            var stepsLabel = Controls.Find("stepsValue", true).FirstOrDefault() as Label;
            var L2Label = Controls.Find("L2OrderValue", true).FirstOrDefault() as Label;
            var W2Label = Controls.Find("W2OrderValue", true).FirstOrDefault() as Label;

            stepsLabel.Text = String.Join(", ", steps);
            L2Label.Text = l2order.ToString();
            W2Label.Text = w2order.ToString();
        }
    }
}
