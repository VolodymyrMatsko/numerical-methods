﻿using FEM_App.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TriangleNet.Data;

namespace FEM_App.Services
{
    public class DrawingServices
    {
        public int scaleIndex{ get; set; }
        public bool ifareain_x { get; set; }
        int padding = 300;
        private Panel _panel;
        public List<List<float>> _CT;
        public List<List<int>> _NT;
        private Graphics _graphics;
        private Pen _elementPen = new Pen(Brushes.Green, 15);
        //private Pen _trianglePen = new Pen(Brushes.Red, 2);

        public DrawingServices(Panel penal)
        {
            _panel = penal;
            _graphics = _panel.CreateGraphics();
            _CT = new List<List<float>>();
            _NT = new List<List<int>>();
        }

        public DrawingServices(Panel panel,List<List<int>> NT, List<List<float>> CT)
        {
            _panel = panel;
            _graphics = _panel.CreateGraphics();
            _CT = CT;
            _NT = NT;
        }

        public void DrawTitles()
        {
            int index = 0;
            for (int i = 0; i < _CT.Count; i++)
            {
                var point = TransferToTopLeftSystem(scaleIndex * _CT[i][0], scaleIndex *_CT[i][1]);
                var title = new NodeTitle(index.ToString(), point);
                _panel.Controls.Add(title.Label);
                ++index;
            }
        }

        public void DrawTriangles(bool drawTitle, Pen trianglePen)
        {
            for (int i = 0; i < _NT.Count; i++)
            {
                List<PointF> vertices = new List<PointF>()
                {
                    TransferToTopLeftSystem(scaleIndex * _CT[_NT[i][0]][0], scaleIndex * _CT[_NT[i][0]][1]),
                    TransferToTopLeftSystem(scaleIndex * _CT[_NT[i][1]][0], scaleIndex * _CT[_NT[i][1]][1]),
                    TransferToTopLeftSystem(scaleIndex * _CT[_NT[i][2]][0], scaleIndex * _CT[_NT[i][2]][1])
                };
                for (int j = 0; j < _NT[0].Count; j++)
                {
                    int next = (j + 1) % 3;
                    _graphics.DrawLine(trianglePen, vertices[j], vertices[next]);
                }
                if (drawTitle)
                {
                    var point = GetTriangleMiddle(vertices);
                    var title = new ElementTitle(i.ToString(), point);
                    _panel.Controls.Add(title.Label);
                    _graphics.DrawEllipse(_elementPen,title.GetFrame(point));
                }
            }
        }

        public void DrawLine(Pen pen, PointF start, PointF end)
        {
            PointF transferedStart = TransferToTopLeftSystem(scaleIndex * start.X, scaleIndex * start.Y);
            PointF transferedEnd = TransferToTopLeftSystem(scaleIndex * end.X, scaleIndex * end.Y);
            _graphics.DrawLine(pen, transferedStart, transferedEnd);
        }

        public void DrawPoints(Pen pen, List<PointF> points)
        {
            for (int i = 0; i < points.Count; i++)
            {
                PointF transferedStart = TransferToTopLeftSystem(scaleIndex * points[i].X, scaleIndex * points[i].Y);
                _graphics.DrawEllipse(pen, new Rectangle((int) transferedStart.X-2, (int) transferedStart.Y-2, 5, 5));
            }
        }

        public void DrawTriangles(Pen pen, List<List<PointF>> trianglePoints)
        {
            for (int i = 0; i < trianglePoints.Count; i++)
            {
                var triangle = trianglePoints[i];
                List<PointF> vertices = new List<PointF>()
                {
                    TransferToTopLeftSystem(scaleIndex * triangle[0].X, scaleIndex * triangle[0].Y),
                    TransferToTopLeftSystem(scaleIndex * triangle[1].X, scaleIndex * triangle[1].Y),
                    TransferToTopLeftSystem(scaleIndex * triangle[2].X, scaleIndex * triangle[2].Y),
                };

                for (int j = 0; j < vertices.Count; j++)
                {
                    int next = (j + 1) % 3;
                    _graphics.DrawLine(pen, vertices[j], vertices[next]);
                }
            }
        }

        public void ClearePanel()
        {
            _graphics.Clear(Color.White);
        }

        private PointF TransferToTopLeftSystem(float x, float y)
        {
            int height = _panel.Height - padding + 200;
            int width = _panel.Width - padding;
            var point = new PointF(x, -y + height);
            //return new PointF(point.X + width / 2, point.Y - height / 2);
            if(ifareain_x)
            {
                return new PointF(point.X + width / 2 - width / 4, point.Y);
            }
            else
            {
                return new PointF(point.X + width / 2, point.Y);
            }
        }

        private PointF GetTriangleMiddle(List<PointF> vertices)
        {
            float middleX = (vertices[0].X + vertices[1].X + vertices[2].X) / 3.0f;
            float middleY = (vertices[0].Y + vertices[1].Y + vertices[2].Y) / 3.0f;
            return new PointF(middleX, middleY);
        }
    }
}
