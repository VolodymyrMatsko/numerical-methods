﻿using FEM_App.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FEM_App.Services
{
    public class AnalysisService
    {
        public enum OrderType
        {
            L2,
            W2
        }

        public List<ResultVectorWithPoint> ComputationSolution1 { get; set; }
        public List<ResultVectorWithPoint> ComputationSolution2 { get; set; }

        public double Step1 { get; set; }
        public double Step2 { get; set; }

        public List<ResultVectorWithPoint> ExactSolution { get; set; }

        public ExactSolutionServices SolutionService { get; set; }

        public AnalysisService(
            List<ResultVectorWithPoint> solution1, 
            List<ResultVectorWithPoint> solution2, 
            double step1,
            double step2,
            List<ResultVectorWithPoint> exact,
            ExactSolutionServices solutionService
        )
        {
            ComputationSolution1 = solution1;
            ComputationSolution2 = solution2;
            Step1                = step1;
            Step2                = step2;
            ExactSolution        = exact.FindAll(item =>
            {
                return solution1.Find(s => s.X == item.X && s.Y == item.Y) != null;
            });
            SolutionService      = solutionService;
        }


        public double GetOrderP(OrderType type)
        {
            var e1 = 0.0;
            var e2 = 0.0;

            switch (type)
            {
                case OrderType.L2:
                    e1 = L2Norm(ComputationSolution1, Step1);
                    e2 = L2Norm(ComputationSolution2, Step2);
                    break;
                case OrderType.W2:
                    e1 = W2Norm(ComputationSolution1, Step1);
                    e2 = W2Norm(ComputationSolution2, Step2);
                    break;
            }

            return (Math.Log(e1) - Math.Log(e2)) / (Math.Log(Step1) - Math.Log(Step2));
        }

        private double L2Norm(List<ResultVectorWithPoint> vector, double step)
        {
            var sum = 0.0;

            for (int i = 0; i < vector.Count; i++)
            {
                sum += Math.Pow(vector[i].result - GetExact(vector[i]), 2);
            }

            return Math.Sqrt(step * sum);
        }

        public double W2Norm(List<ResultVectorWithPoint> vector, double step)
        {
            var sum = 0.0;

            for (int i = 1; i < vector.Count - 1; i++)
            {
                var temp = (vector[i + 1].result - vector[i - 1].result) / (2 * step);

                sum += Math.Pow(temp - ExactDerivative(vector[i]), 2);
            }

            sum += Math.Pow((vector[1].result - vector[0].result) / step - ExactDerivative(vector[0]), 2);

            sum += Math.Pow((vector[vector.Count - 1].result - vector[vector.Count - 2].result) / step -
                                    ExactDerivative(vector[vector.Count - 1]), 2);

            for (int i = 0; i < vector.Count; i++)
            {
                //sum += (Math.Pow(vector[i].result - ExactSolution[i].result, 2) * step);
            }

            return step * Math.Sqrt(sum);
        }

        private double ExactDerivative(ResultVectorWithPoint point)
        {
            var derivStep = 0.01f;

            var points = new List<List<float>>()
            {
                new List<float>() { point.X - derivStep, point.Y },
                new List<float>() { point.X, point.Y },
                new List<float>() { point.X + derivStep, point.Y }
            };

            var temp = SolutionService.GetSolutionToPoint(points);

            return (temp[0].result - 2 * temp[1].result + temp[2].result) / Math.Pow(derivStep, 2);
        }

        private double GetExact(ResultVectorWithPoint point)
        {
            var points = new List<List<float>>()
            {
                new List<float>() { point.X, point.Y },
            };

            return SolutionService.GetSolutionToPoint(points).First().result;
        }
    }
}
