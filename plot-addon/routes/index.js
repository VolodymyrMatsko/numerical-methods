var express = require('express');
var router = express.Router();

const fs = require('fs')
const path = require('path')

const dirs = p => fs.readdirSync(p).filter(f => fs.statSync(path.join(p, f)).isDirectory())

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/result', function(req, res, next) {
  result_path = path.join(__dirname, '../../FEM-App/FEM-App/Store/Complete/result.json')
  json = fs.readFile(result_path, 'utf8', (err, data) => {
    res.json(data)  
  })
})

module.exports = router;
