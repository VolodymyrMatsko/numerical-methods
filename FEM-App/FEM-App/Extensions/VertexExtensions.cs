﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TriangleNet.Data;

namespace FEM_App.Extensions
{
    public static class VertexExtensions
    {
        public static List<PointF> ToPointFList(this ICollection<Vertex> listVertex)
        {
            List<PointF> res = new List<PointF>();
            var vertices = listVertex.ToList();
            for (int i = 0; i < vertices.Count; i++)
            {
                res.Add(new PointF((float)vertices[i].X, (float)vertices[i].Y));
            }
            return res;
        }

        

        public static PointF ToPointF(this Vertex vertex)
        {
            PointF res = new PointF();
            res.X = (float)vertex.X;
            res.Y = (float)vertex.Y;
            return res;
        }

        public static PointF ToPointF(this TriangleNet.Geometry.Point point)
        {
            PointF res = new PointF();
            res.X = (float)point.X;
            res.Y = (float)point.Y;
            return res;
        }
    }
}
