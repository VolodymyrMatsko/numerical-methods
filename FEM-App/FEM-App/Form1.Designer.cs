﻿namespace FEM_App
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.addLabelBtn = new System.Windows.Forms.Button();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.richTextBox2 = new System.Windows.Forms.RichTextBox();
            this.richTextBox3 = new System.Windows.Forms.RichTextBox();
            this.elementsTitle = new System.Windows.Forms.CheckBox();
            this.nodesTitle = new System.Windows.Forms.CheckBox();
            this.trackBar1 = new System.Windows.Forms.TrackBar();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.a11tb = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.a22tb = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.dtb = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.ftb = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.calculateBtn = new System.Windows.Forms.Button();
            this.tbError_abs = new System.Windows.Forms.TextBox();
            this.tbError_rel = new System.Windows.Forms.TextBox();
            this.btDraw = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.Calculation = new System.Windows.Forms.TabPage();
            this.tbCutPoints = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.Analysis = new System.Windows.Forms.TabPage();
            this.W2OrderValue = new System.Windows.Forms.Label();
            this.L2OrderValue = new System.Windows.Forms.Label();
            this.stepsValue = new System.Windows.Forms.Label();
            this.labelOrderW2 = new System.Windows.Forms.Label();
            this.labelOrderL = new System.Windows.Forms.Label();
            this.labelSteps = new System.Windows.Forms.Label();
            this.labelAnalysis = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.Calculation.SuspendLayout();
            this.Analysis.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.AutoScrollMinSize = new System.Drawing.Size(1400, 1400);
            this.panel1.Location = new System.Drawing.Point(21, 5);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(961, 711);
            this.panel1.TabIndex = 0;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(0, 0);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            // 
            // addLabelBtn
            // 
            this.addLabelBtn.Location = new System.Drawing.Point(884, 23);
            this.addLabelBtn.Name = "addLabelBtn";
            this.addLabelBtn.Size = new System.Drawing.Size(75, 23);
            this.addLabelBtn.TabIndex = 2;
            this.addLabelBtn.Text = "Add Labels";
            this.addLabelBtn.UseVisualStyleBackColor = true;
            this.addLabelBtn.Click += new System.EventHandler(this.addLabelBtn_Click);
            // 
            // richTextBox1
            // 
            this.richTextBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextBox1.Location = new System.Drawing.Point(3, 25);
            this.richTextBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(246, 390);
            this.richTextBox1.TabIndex = 4;
            this.richTextBox1.Text = "";
            // 
            // richTextBox2
            // 
            this.richTextBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextBox2.Location = new System.Drawing.Point(3, 25);
            this.richTextBox2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.richTextBox2.Name = "richTextBox2";
            this.richTextBox2.Size = new System.Drawing.Size(238, 390);
            this.richTextBox2.TabIndex = 4;
            this.richTextBox2.Text = "";
            // 
            // richTextBox3
            // 
            this.richTextBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextBox3.Location = new System.Drawing.Point(3, 25);
            this.richTextBox3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.richTextBox3.Name = "richTextBox3";
            this.richTextBox3.Size = new System.Drawing.Size(261, 390);
            this.richTextBox3.TabIndex = 4;
            this.richTextBox3.Text = "";
            // 
            // elementsTitle
            // 
            this.elementsTitle.AutoSize = true;
            this.elementsTitle.Location = new System.Drawing.Point(312, 32);
            this.elementsTitle.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.elementsTitle.Name = "elementsTitle";
            this.elementsTitle.Size = new System.Drawing.Size(115, 29);
            this.elementsTitle.TabIndex = 9;
            this.elementsTitle.Text = "Elements";
            this.elementsTitle.UseVisualStyleBackColor = true;
            this.elementsTitle.CheckedChanged += new System.EventHandler(this.elementsTitle_CheckedChanged);
            // 
            // nodesTitle
            // 
            this.nodesTitle.AutoSize = true;
            this.nodesTitle.Location = new System.Drawing.Point(205, 31);
            this.nodesTitle.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.nodesTitle.Name = "nodesTitle";
            this.nodesTitle.Size = new System.Drawing.Size(91, 29);
            this.nodesTitle.TabIndex = 10;
            this.nodesTitle.Text = "Nodes";
            this.nodesTitle.UseVisualStyleBackColor = true;
            this.nodesTitle.CheckedChanged += new System.EventHandler(this.nodesTitle_CheckedChanged);
            // 
            // trackBar1
            // 
            this.trackBar1.Location = new System.Drawing.Point(3, 654);
            this.trackBar1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.trackBar1.Name = "trackBar1";
            this.trackBar1.Size = new System.Drawing.Size(760, 56);
            this.trackBar1.TabIndex = 11;
            this.trackBar1.Scroll += new System.EventHandler(this.trackBar1_Scroll);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(568, 32);
            this.textBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(141, 30);
            this.textBox1.TabIndex = 12;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(461, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(97, 25);
            this.label1.TabIndex = 13;
            this.label1.Text = "Max Area";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.richTextBox1);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox1.Location = new System.Drawing.Point(7, 6);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Size = new System.Drawing.Size(252, 417);
            this.groupBox1.TabIndex = 14;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "CT";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.richTextBox2);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox2.Location = new System.Drawing.Point(265, 6);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox2.Size = new System.Drawing.Size(244, 417);
            this.groupBox2.TabIndex = 15;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "NT";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.richTextBox3);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox3.Location = new System.Drawing.Point(512, 6);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox3.Size = new System.Drawing.Size(267, 417);
            this.groupBox3.TabIndex = 16;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "NTG";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.comboBox1);
            this.groupBox4.Controls.Add(this.elementsTitle);
            this.groupBox4.Controls.Add(this.nodesTitle);
            this.groupBox4.Controls.Add(this.label1);
            this.groupBox4.Controls.Add(this.textBox1);
            this.groupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox4.Location = new System.Drawing.Point(7, 539);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox4.Size = new System.Drawing.Size(756, 110);
            this.groupBox4.TabIndex = 17;
            this.groupBox4.TabStop = false;
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Area 1",
            "Area 2",
            "Area 3",
            "Area 4",
            "Area 5"});
            this.comboBox1.Location = new System.Drawing.Point(20, 30);
            this.comboBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(177, 33);
            this.comboBox1.TabIndex = 14;
            this.comboBox1.SelectedValueChanged += new System.EventHandler(this.comboBox1_SelectedValueChanged);
            // 
            // a11tb
            // 
            this.a11tb.Location = new System.Drawing.Point(41, 446);
            this.a11tb.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.a11tb.Name = "a11tb";
            this.a11tb.Size = new System.Drawing.Size(75, 22);
            this.a11tb.TabIndex = 18;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(-1, 449);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 17);
            this.label2.TabIndex = 19;
            this.label2.Text = "a11";
            // 
            // a22tb
            // 
            this.a22tb.Location = new System.Drawing.Point(41, 478);
            this.a22tb.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.a22tb.Name = "a22tb";
            this.a22tb.Size = new System.Drawing.Size(75, 22);
            this.a22tb.TabIndex = 20;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(-1, 485);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(32, 17);
            this.label3.TabIndex = 21;
            this.label3.Text = "a22";
            // 
            // dtb
            // 
            this.dtb.Location = new System.Drawing.Point(172, 442);
            this.dtb.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dtb.Name = "dtb";
            this.dtb.Size = new System.Drawing.Size(61, 22);
            this.dtb.TabIndex = 22;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(147, 446);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(16, 17);
            this.label4.TabIndex = 23;
            this.label4.Text = "d";
            // 
            // ftb
            // 
            this.ftb.Location = new System.Drawing.Point(172, 478);
            this.ftb.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ftb.Name = "ftb";
            this.ftb.Size = new System.Drawing.Size(61, 22);
            this.ftb.TabIndex = 24;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(151, 481);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(12, 17);
            this.label5.TabIndex = 25;
            this.label5.Text = "f";
            // 
            // calculateBtn
            // 
            this.calculateBtn.Location = new System.Drawing.Point(252, 436);
            this.calculateBtn.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.calculateBtn.Name = "calculateBtn";
            this.calculateBtn.Size = new System.Drawing.Size(100, 28);
            this.calculateBtn.TabIndex = 26;
            this.calculateBtn.Text = "calculate";
            this.calculateBtn.UseVisualStyleBackColor = true;
            this.calculateBtn.Click += new System.EventHandler(this.calculateBtn_Click);
            // 
            // tbError_abs
            // 
            this.tbError_abs.Location = new System.Drawing.Point(252, 479);
            this.tbError_abs.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbError_abs.Name = "tbError_abs";
            this.tbError_abs.Size = new System.Drawing.Size(116, 22);
            this.tbError_abs.TabIndex = 27;
            // 
            // tbError_rel
            // 
            this.tbError_rel.Location = new System.Drawing.Point(252, 507);
            this.tbError_rel.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbError_rel.Name = "tbError_rel";
            this.tbError_rel.Size = new System.Drawing.Size(116, 22);
            this.tbError_rel.TabIndex = 28;
            // 
            // btDraw
            // 
            this.btDraw.Location = new System.Drawing.Point(383, 436);
            this.btDraw.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btDraw.Name = "btDraw";
            this.btDraw.Size = new System.Drawing.Size(100, 28);
            this.btDraw.TabIndex = 29;
            this.btDraw.Text = "draw ";
            this.btDraw.UseVisualStyleBackColor = true;
            this.btDraw.Click += new System.EventHandler(this.btDraw_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.Calculation);
            this.tabControl1.Controls.Add(this.Analysis);
            this.tabControl1.Location = new System.Drawing.Point(989, 5);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(833, 742);
            this.tabControl1.TabIndex = 0;
            // 
            // Calculation
            // 
            this.Calculation.Controls.Add(this.button2);
            this.Calculation.Controls.Add(this.tbCutPoints);
            this.Calculation.Controls.Add(this.label7);
            this.Calculation.Controls.Add(this.btDraw);
            this.Calculation.Controls.Add(this.groupBox1);
            this.Calculation.Controls.Add(this.tbError_rel);
            this.Calculation.Controls.Add(this.trackBar1);
            this.Calculation.Controls.Add(this.tbError_abs);
            this.Calculation.Controls.Add(this.groupBox2);
            this.Calculation.Controls.Add(this.calculateBtn);
            this.Calculation.Controls.Add(this.groupBox3);
            this.Calculation.Controls.Add(this.label5);
            this.Calculation.Controls.Add(this.groupBox4);
            this.Calculation.Controls.Add(this.ftb);
            this.Calculation.Controls.Add(this.a11tb);
            this.Calculation.Controls.Add(this.label4);
            this.Calculation.Controls.Add(this.label2);
            this.Calculation.Controls.Add(this.dtb);
            this.Calculation.Controls.Add(this.a22tb);
            this.Calculation.Controls.Add(this.label3);
            this.Calculation.Location = new System.Drawing.Point(4, 25);
            this.Calculation.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Calculation.Name = "Calculation";
            this.Calculation.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Calculation.Size = new System.Drawing.Size(825, 713);
            this.Calculation.TabIndex = 0;
            this.Calculation.Text = "Calculation";
            this.Calculation.UseVisualStyleBackColor = true;
            // 
            // tbCutPoints
            // 
            this.tbCutPoints.Location = new System.Drawing.Point(129, 518);
            this.tbCutPoints.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbCutPoints.Name = "tbCutPoints";
            this.tbCutPoints.Size = new System.Drawing.Size(104, 22);
            this.tbCutPoints.TabIndex = 31;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(9, 522);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(110, 17);
            this.label7.TabIndex = 30;
            this.label7.Text = "cutPointsCounts";
            // 
            // Analysis
            // 
            this.Analysis.Controls.Add(this.W2OrderValue);
            this.Analysis.Controls.Add(this.L2OrderValue);
            this.Analysis.Controls.Add(this.stepsValue);
            this.Analysis.Controls.Add(this.labelOrderW2);
            this.Analysis.Controls.Add(this.labelOrderL);
            this.Analysis.Controls.Add(this.labelSteps);
            this.Analysis.Controls.Add(this.labelAnalysis);
            this.Analysis.Location = new System.Drawing.Point(4, 25);
            this.Analysis.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Analysis.Name = "Analysis";
            this.Analysis.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Analysis.Size = new System.Drawing.Size(825, 713);
            this.Analysis.TabIndex = 1;
            this.Analysis.Text = "Analysis";
            this.Analysis.UseVisualStyleBackColor = true;
            // 
            // W2OrderValue
            // 
            this.W2OrderValue.AutoSize = true;
            this.W2OrderValue.Location = new System.Drawing.Point(165, 183);
            this.W2OrderValue.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.W2OrderValue.Name = "W2OrderValue";
            this.W2OrderValue.Size = new System.Drawing.Size(13, 17);
            this.W2OrderValue.TabIndex = 6;
            this.W2OrderValue.Text = "-";
            // 
            // L2OrderValue
            // 
            this.L2OrderValue.AutoSize = true;
            this.L2OrderValue.Location = new System.Drawing.Point(165, 123);
            this.L2OrderValue.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.L2OrderValue.Name = "L2OrderValue";
            this.L2OrderValue.Size = new System.Drawing.Size(13, 17);
            this.L2OrderValue.TabIndex = 5;
            this.L2OrderValue.Text = "-";
            // 
            // stepsValue
            // 
            this.stepsValue.AutoSize = true;
            this.stepsValue.Location = new System.Drawing.Point(165, 69);
            this.stepsValue.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.stepsValue.Name = "stepsValue";
            this.stepsValue.Size = new System.Drawing.Size(13, 17);
            this.stepsValue.TabIndex = 4;
            this.stepsValue.Text = "-";
            // 
            // labelOrderW2
            // 
            this.labelOrderW2.AutoSize = true;
            this.labelOrderW2.Location = new System.Drawing.Point(27, 183);
            this.labelOrderW2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelOrderW2.Name = "labelOrderW2";
            this.labelOrderW2.Size = new System.Drawing.Size(93, 17);
            this.labelOrderW2.TabIndex = 3;
            this.labelOrderW2.Text = "Order by W2:";
            // 
            // labelOrderL
            // 
            this.labelOrderL.AutoSize = true;
            this.labelOrderL.Location = new System.Drawing.Point(27, 123);
            this.labelOrderL.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelOrderL.Name = "labelOrderL";
            this.labelOrderL.Size = new System.Drawing.Size(88, 17);
            this.labelOrderL.TabIndex = 2;
            this.labelOrderL.Text = "Order by L2:";
            // 
            // labelSteps
            // 
            this.labelSteps.AutoSize = true;
            this.labelSteps.Location = new System.Drawing.Point(27, 69);
            this.labelSteps.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelSteps.Name = "labelSteps";
            this.labelSteps.Size = new System.Drawing.Size(48, 17);
            this.labelSteps.TabIndex = 1;
            this.labelSteps.Text = "Steps:";
            // 
            // labelAnalysis
            // 
            this.labelAnalysis.AutoSize = true;
            this.labelAnalysis.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAnalysis.Location = new System.Drawing.Point(24, 17);
            this.labelAnalysis.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelAnalysis.Name = "labelAnalysis";
            this.labelAnalysis.Size = new System.Drawing.Size(212, 31);
            this.labelAnalysis.TabIndex = 0;
            this.labelAnalysis.Text = "Order analysis:";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(383, 476);
            this.button2.Margin = new System.Windows.Forms.Padding(4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(100, 28);
            this.button2.TabIndex = 32;
            this.button2.Text = "draw ";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1827, 752);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.panel1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.Calculation.ResumeLayout(false);
            this.Calculation.PerformLayout();
            this.Analysis.ResumeLayout(false);
            this.Analysis.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button addLabelBtn;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.RichTextBox richTextBox2;
        private System.Windows.Forms.RichTextBox richTextBox3;
        private System.Windows.Forms.CheckBox elementsTitle;
        private System.Windows.Forms.CheckBox nodesTitle;
        private System.Windows.Forms.TrackBar trackBar1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.TextBox a11tb;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox a22tb;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox dtb;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox ftb;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button calculateBtn;
        private System.Windows.Forms.TextBox tbError_abs;
        private System.Windows.Forms.TextBox tbError_rel;
        private System.Windows.Forms.Button btDraw;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage Calculation;
        private System.Windows.Forms.TabPage Analysis;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbCutPoints;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label labelSteps;
        private System.Windows.Forms.Label labelAnalysis;
        private System.Windows.Forms.Label labelOrderL;
        private System.Windows.Forms.Label labelOrderW2;
        private System.Windows.Forms.Label W2OrderValue;
        private System.Windows.Forms.Label L2OrderValue;
        private System.Windows.Forms.Label stepsValue;
        private System.Windows.Forms.Button button2;
    }
}

