﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace FEM_App.Models
{
    public class CustomArea
    {
        public int Number { get; set; }
        public List<PointF> Points { get; set; }
        public double[] Beta { get; set; }
        public double[] Sigma { get; set; }

        public List<List<double>> Uc { get; set; }
    }
}
