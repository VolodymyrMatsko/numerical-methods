﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TriangleNet.Geometry;
using FEM_App.Models;

namespace FEM_App.Services
{
    public class MatrixService
    {
        private List<List<float>> _ct;
        private List<List<int>> _nt;
		private List<List<Edge>> _ntg;
        private Equation _equation;
        public List<List<double>> _A;
        public List<double> _B;

		public MatrixService(List<List<float>> ct, List<List<int>> nt, List<List<Edge>> ntg, Equation equation)
        {
            _ct = ct;
            _nt = nt;
			_ntg = ntg;
            _equation = equation;
            _A = new List<List<double>>();
        }

        public void PrintMeMatrices()
        {
            List<List<double>> initialMatrix = new List<List<double>>()
            {
                new List<double>() {2.0, 1.0, 1.0 },
                new List<double>() {1.0, 2.0, 1.0 },
                new List<double>() {1.0, 1.0, 2.0 }
            };

			var timestamp = DateTime.Now.Ticks;

            for (int i = 0; i < _nt.Count; i++)
            {
                double delta = 2*GetArea(i);
                double index = _equation.D * delta / 24.00;
                var result = MultiplyMatrixOnConstant(initialMatrix, index);
                FillAMatrix(i, result);
				MatrixPrinter.Call(result, i.ToString(), "Me", timestamp);
            }
        }

		public void PrintPeMatrices(BoundaryCondition condition)
		{
			List<List<double>> initialMatrix = new List<List<double>>()
			{
				new List<double>() { 2.0,1.0 },
				new List<double>() { 1.0,2.0 }
			};

			var timestamp = DateTime.Now.Ticks;

			for (int i = 0; i < _ntg.Count; i++) {
				for(int j = 0; j < _ntg[i].Count; j++) {
					var gamma = GetEdgeLength(i, j);
					var constant = (-1) * (condition.Sigma[i] / condition.Beta[i]) * (gamma / 6.0);
					var result = MultiplyMatrixOnConstant(initialMatrix, constant);
					MatrixPrinter.Call(result, $"{i}_{j}", "Pe", timestamp);
                    FillAMatrixPe(i,j,result);
                }
			}
		}

        public void PrintQeMatrices()
        {
            List<List<double>> initialMatrixforMe = new List<List<double>>()
            {
                new List<double>() {2.0, 1.0, 1.0 },
                new List<double>() {1.0, 2.0, 1.0 },
                new List<double>() {1.0, 1.0, 2.0 }
            };

            var timestamp = DateTime.Now.Ticks;
            List<List<double>> fe = new List<List<double>>() // чому це матриця  а не вектор ???
            {
                new List<double>()
                {
                    _equation.F(0,0,0),
                    _equation.F(0,0,0),
                    _equation.F(0,0,0)
                }
            };

            for (int i = 0; i < _nt.Count; i++)
            {
                //double index = -1* _equation.D * GetArea(i) / 24.00;
                double delta = 2 * GetArea(i);
                double index = (-1) * delta / 24.00; // забрав -1 бо f задається з -
                var result = MultiplyMatrixOnConstant(initialMatrixforMe, index);
                result = MultiplyMatrixOnVector(result, fe[0]);
                MatrixPrinter.Call(result, i.ToString(), "Qe", timestamp);
                FillBVector(i, result[0]);
            }
        }

        public void PrintReMatrices(BoundaryCondition condition)
		{

			var timestamp = DateTime.Now.Ticks;

			for (int i = 0; i < _ntg.Count; i++) {
			    List<List<double>> initialMatrix = new List<List<double>>()
			    {
				    new List<double>() { condition.Uc[i][0] },
				    new List<double>() { condition.Uc[i][1] }
			    };
				for(int j = 0; j < _ntg[i].Count; j++) {
					var gamma = GetEdgeLength(i, j);
					var constant = (-1)*(condition.Sigma[i] / condition.Beta[i]) * (gamma / 2.0); // забрав -1 бо f задається з -
                    var result = MultiplyMatrixOnConstant(initialMatrix, constant);
					MatrixPrinter.Call(result, $"{i}_{j}", "Re", timestamp);
                    FillBVectorRe(i,j, new List<double> { result[0][0], result[1][0] });
                }
			}
		}

        public void PrintKeMatrices()
        {
            var timestamp = DateTime.Now.Ticks;

            for (int k = 0; k < _nt.Count; k++)
            {
                double delta = 2 * GetArea(k);
                double constant = 1.0 / (delta * 2.0);
                List<List<double>> matrix = new List<List<double>>();
                for (int i = 0; i < 3; i++)
                {
                    List<double> line = new List<double>();
                    for (int j = 0; j < 3; j++)
                    {
                        double value1 = _equation.A11 * GetB(k, i) * GetB(k, j);
                        double value2 = _equation.A22 * GetC(k, i) * GetC(k, j);
                        line.Add(value1+value2);
                    }
                    matrix.Add(line);
                }
                var result = MultiplyMatrixOnConstant(matrix, constant);
                FillAMatrix(k, result);
                MatrixPrinter.Call(result, k.ToString(), "Ke", timestamp);
            }
        }

		public void PrintAB()
		{
			var timestamp = DateTime.Now.Ticks;
			MatrixPrinter.Call(_A, "a", "AB", timestamp);
			MatrixPrinter.Call(new List<List<double>>() { _B }, "b", "AB", timestamp);
		}

        public void PrintAMatrix()
        {
            var timestamp = DateTime.Now.Ticks;
            MatrixPrinter.Call(_A, String.Empty, "A", timestamp);
        }

        public void PrintBVector()
        {
            var timestamp = DateTime.Now.Ticks;
            MatrixPrinter.Call(new List<List<double>>() { _B }, String.Empty, "B", timestamp);
        }

        public  void FillAMatrix(int elemNum, List<List<double>> matrix)
        {
            for (int k1 = 0; k1 < matrix.Count; k1++)
            {
                for (int k2 = 0; k2 < matrix.Count; k2++)
                {
                    int _i = _nt[elemNum][k1];
                    int _j = _nt[elemNum][k2];
                    _A[_i][_j] += matrix[k1][k2];
                }
            }
        }
        public void FillAMatrixPe(int numBound,int numelem, List<List<double>> matrix)
        {
            List<int> indexis = new List<int> { _ntg[numBound][numelem].P0, _ntg[numBound][numelem].P1 };
            int k1 = 0;
            foreach (int _i in indexis)
            {
                int k2 = 0;
                foreach (int _j in indexis)
                {
                    _A[_i][_j] += matrix[k1][k2];
                    k2++;
                }
                k1++;
            }
            //for (int k1 = 0; k1 < indexis.Count; k1++)
            //{
            //    for (int k2 = 0; k2 < indexis.Count; k2++)
            //    {
            //        int _i = _ntg[numBound][numelem].P0;
            //        int _j = _ntg[numBound][numelem].P1;
            //        _A[_i][_j] += matrix[k1][k2];
            //    }
            //}
        }

        public void FillBVectorRe(int numBound, int numelem, List<double> vector)
        {
            List<int> indexis = new List<int> { _ntg[numBound][numelem].P0, _ntg[numBound][numelem].P1 };
            int k1 = 0;
            foreach (int _i in indexis)
            {
                _B[_i] += vector[k1];
                k1++;
            }
        }
        public void FillBVector(int elemNum, List<double> vector)
        {
            for (int k = 0; k < vector.Count; k++)
            {
                int _i = _nt[elemNum][k];
                _B[_i] += vector[k];
            }


            //for (int k = 0; k < vector.Count; k++)
            //{
            //    int _i = _nt[elemNum][k];
            //    _B[_i] += vector[k];
            //}
        }

        public double[] GetResult(BoundaryCondition boundaryConditions)
        {
            InitMatrixAVectorB(_ct.Count);
            PrintMeMatrices();

            //var boundaryConditions = AreasProvider.GetCondition(_idArea);

            PrintPeMatrices(boundaryConditions);
            PrintReMatrices(boundaryConditions);
            PrintKeMatrices();
            PrintQeMatrices();

            PrintAB();

            double[,] mtr = new double[_A.Count, _A.Count];
            for (int i = 0; i < _A.Count; i++)
            {
                for (int j = 0; j < _A.Count; j++)
                {
                    mtr[i, j] = _A[i][j];
                }
            }

            GaussSolver solver = new GaussSolver(mtr, _B.ToArray());
            solver.GaussSolve();

            //List<ResultVectorWithPoint> resultOnBounde = Helper.GetResultOnBounde(solver.XVector, CT);
            //_resultOnLine = Helper.GetResultOnLine(solver.XVector, CT, Line);


            var timestamp = DateTime.Now.Ticks;
            MatrixPrinter.CallResult(new List<double>(solver.XVector), _ct, "result", "RESULT", timestamp);

            // save complete result data as json
            // for futher creation of 3d plot
            CompleteResultProvider.Call(_ct, new List<double>(solver.XVector), DateTime.Now.Ticks);

            return solver.XVector;

            //_drawingService = new DrawingServices(panel1, _nt, _ct);
            //_drawingService.scaleIndex = _scaleIndex;
            //_drawingService.ifareain_x = _ifareain_x;
            //Draw();
            //if (elementsTitle.Checked) { elementsTitle.Checked = false; }
            //if (nodesTitle.Checked) { nodesTitle.Checked = false; }
            //WriteArrays();

            //ExactSolutionServices exactSolutionServices = new ExactSolutionServices(equation, boundaryConditions, new List<double>() { 0.0, 1.0 });
            //_resultExact = exactSolutionServices.GetSolutionToPoint(Helper.GetPointInLine(_ct, Line));


            //List<double> res = Helper.GetDerivToNormal(resultOnBounde);

            //tbError_abs.Text = string.Format("{0:n5}", Helper.GetErrorAbs(_resultOnLine, _resultExact));
            //tbError_rel.Text = string.Format("{0:n5}", Helper.GetErrorRel(_resultOnLine, _resultExact));
        }

        public void InitMatrixAVectorB(int size)
        {
            _A = new List<List<double>>();
            _B = new List<double>();
            for (int i = 0; i < size; i++)
            {
                _A.Add(new List<double>());
                for (int j = 0; j < size; j++)
                {
                    _A[i].Add(0);
                }
                _B.Add(0);
            }
        }

        private List<List<double>> MultiplyMatrixOnConstant(List<List<double>> matrix, double constant)
        {
            List<List<double>> result = new List<List<double>>();
            for (int i = 0; i < matrix.Count; i++)
            {
                result.Add(new List<double>());
                for (int j = 0; j < matrix[0].Count; j++)
                {
                    result[i].Add(matrix[i][j] * constant);
                }
            }
            return result;
        }
        private List<List<double>> MultiplyMatrixOnVector(List<List<double>> matrix, List<double> vector)
        {
            List<List<double>> res_vector = new List<List<double>>();
            res_vector.Add(new List<double>());  
            for (int i = 0; i < matrix.Count; i++)
            {
                double s = 0.0;
                for (int j = 0; j < vector.Count; j++)
                {
                    s += matrix[i][j] * vector[j];
                }
                res_vector[0].Add(s);
            }
            return res_vector;
        }
        private double GetEdgeLength(int edgeNumber, int intervalNumber)
		{
			var x0 = _ct [_ntg [edgeNumber][intervalNumber].P0][0];
			var y0 = _ct [_ntg [edgeNumber][intervalNumber].P0][1];
			var x1 = _ct [_ntg [edgeNumber][intervalNumber].P1][0];
			var y1 = _ct [_ntg [edgeNumber][intervalNumber].P1][1];

			var temp1 = Math.Pow (x1 - x0, 2);
			var temp2 = Math.Pow (y1 - y0, 2);

			return (Math.Sqrt(temp1 + temp2));
		}
        private double GetArea(int trNumber)
        {
            var x0 = _ct[_nt[trNumber][0]][0];
            var y0 = _ct[_nt[trNumber][0]][1];
            var x1 = _ct[_nt[trNumber][1]][0];
            var y1 = _ct[_nt[trNumber][1]][1];
            var x2 = _ct[_nt[trNumber][2]][0];
            var y2 = _ct[_nt[trNumber][2]][1];
            var a = Math.Sqrt(Math.Pow(x1 - x0, 2) + Math.Pow(y1 - y0, 2));
            var b = Math.Sqrt(Math.Pow(x2 - x1, 2) + Math.Pow(y2 - y1, 2));
            var c = Math.Sqrt(Math.Pow(x2 - x0, 2) + Math.Pow(y2 - y0, 2));
            return Math.Abs((x1 * y2 + x2 * y0 + x0 * y1 - x1 * y0 - x2 * y1 - x0 * y2) / 2.0);
            //var p = (a + b + c) / 2.0;
            //return 2*(Math.Sqrt(p * (p - a) * (p - b) * (p - c)));
        }
        private double GetB(int trNumber,int index)
        {
            double res = 0;
            switch (index)
            {
                case 0:
                    res = _ct[_nt[trNumber][1]][1] - _ct[_nt[trNumber][2]][1];//x(j)2 - x(m)2
                    break;
                case 1:
                    res = _ct[_nt[trNumber][2]][1] - _ct[_nt[trNumber][0]][1];//x(m)2 - x(i)2
                    break;
                case 2:
                    res = _ct[_nt[trNumber][0]][1] - _ct[_nt[trNumber][1]][1];//x(i)2 - x(j)2
                    break;
            }
            return res;
        }
        private double GetC(int trNumber, int index)
        {
            double res = 0;
            switch (index)
            {
                case 0:
                    res = _ct[_nt[trNumber][2]][0] - _ct[_nt[trNumber][1]][0];//x(m)1 - x(j)1
                    break;
                case 1:
                    res = _ct[_nt[trNumber][0]][0] - _ct[_nt[trNumber][2]][0];//x(i)1 - x(j)1
                    break;
                case 2:
                    res = _ct[_nt[trNumber][1]][0] - _ct[_nt[trNumber][0]][0];//x(j)1 - x(i)1
                    break;
            }
            return res;
        }
    }
}
