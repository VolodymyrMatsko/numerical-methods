﻿using System;
using System.Collections.Generic;
using System.Drawing;
using FEM_App.Models;
using Newtonsoft.Json;

namespace FEM_App.Store
{
    public static class AreasProvider
    {
        private const string filename = "../../Store/Data/areas.json";

        public static List<PointF> GetArea(int number)
        {
            var allAreas = GetAllAreas();
            return allAreas[number].Points;
        }

        public static BoundaryCondition GetCondition(int number)
        {
            var allAreas = GetAllAreas();
            //return new BoundaryCondition()
            //{
            //    Beta = allAreas[number].Beta,
            //    Sigma = allAreas[number].Sigma,
            //    Uc = allAreas[number].Uc
            //};
            return new BoundaryCondition()
            {
                Beta = new double[] { 1.0, 0.0001, 1.0, 0.0001 },
                Sigma = new double[] { 0.0001, 1.0, 0.0001, 1.0 },
                Uc = allAreas[number].Uc
            };
        }

        private static List<CustomArea> GetAllAreas()
        {
            var json = System.IO.File.ReadAllText(filename);
            var result = JsonConvert.DeserializeObject<List<CustomArea>>(json);
            return result;
        }
    }
}
