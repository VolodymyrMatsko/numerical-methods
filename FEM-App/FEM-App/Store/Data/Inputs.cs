﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FEM_App.Store.Data
{
    public static class Inputs
    {
        public static Line FixedLine { get; set; } = new Line(0, 1, 1, 1);
    }

    public class Line
    {
        public PointF Start { get; set; }
        public PointF End { get; set; }

        public Line(float startX, float startY, float endX, float endY)
        {
            Start = new PointF(startX, startY);
            End = new PointF(endX, endY);
        }
    }
}
