clc;  clear all;
fid=fopen('RESULT.txt');
i=1;
while ~(feof(fid))
    m=fgetl(fid)
    if ~isempty(m)
        m=strrep(m,',','.');
        a(i,:)=str2num(m);
        i=i+1;
    end 
end
fclose(fid);
disp(a)
x=a(:,1);
y=a(:,2);
z=a(:,3);
[qx,qy] = meshgrid(linspace(min(x),max(x)),linspace(min(y),max(y)));
F = TriScatteredInterp(x,y,z);
qz = F(qx,qy);

surf(qx,qy,qz)