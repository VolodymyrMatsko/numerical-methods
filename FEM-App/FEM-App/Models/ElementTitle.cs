﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FEM_App.Models
{
    class ElementTitle
    {
        private int _size = 8;
        public Label Label { get; }
        public ElementTitle(string text, PointF location)
        {
            location.X -= _size / 2;
            location.Y -= _size / 2;
            Label = new Label();
            Label.Text = text;
            Label.BackColor = Color.Green;
            Label.Height = 2*_size;
            Label.Width = 19;
            Label.Location = new Point((int)location.X - _size/2, (int)location.Y - _size / 4);
        }

        public RectangleF GetFrame(PointF point)
        {
            point.X -= _size / 2;
            point.Y -= _size / 2;
            return new RectangleF(point, new SizeF(_size, _size));
        }
    }
}
