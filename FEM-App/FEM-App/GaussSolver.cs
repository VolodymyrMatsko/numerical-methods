﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FEM_App
{
    class GaussSolver
    {
		public double[] XVector { get; set; }
		public double[] BVector { get; set; }
		public double[,] AMatrix { get; set; }
		public int Size { get; set; }

		public GaussSolver(double[,] amatrix, double[] bvector)
        {
			this.AMatrix = (double[,])amatrix.Clone();
			this.BVector = (double[])bvector.Clone();
			this.XVector = new double[bvector.Length];
			this.Size = bvector.Length;
        }

        public void GaussSolve()
        {
            int[] index = InitIndex();
            ForwardGauss(index);
            BackwardGauss(index);
        }
			
        private int[] InitIndex()
        {
			int[] index = new int[Size];
			for (int i = 0; i < index.Length; ++i) index [i] = i;
			return index;
        }
			
        private void ForwardGauss(int[] index)
        {
			for (int i = 0; i < Size; ++i)
            {
                double r = FindR(i, index);

				for (int j = 0; j < Size; ++j) AMatrix[i, j] /= r;

				BVector[i] /= r;

				for (int k = i + 1; k < Size; ++k)
                {
					double p = AMatrix[k, index[i]];
					for (int j = i; j < Size; ++j) AMatrix[k, index[j]] -= AMatrix[i, index[j]] * p;
					BVector[k] -= BVector[i] * p;
					AMatrix[k, index[i]] = 0.0;
                }
            }
        }
			
        private void BackwardGauss(int[] index)
        {
			for (int i = Size - 1; i >= 0; --i)
            {
				double x_i = BVector[i];
				for (int j = i + 1; j < Size; ++j) x_i -= XVector[index[j]] * AMatrix[i, index[j]];
				XVector[index[i]] = x_i;
            }
        }
			
        private double FindR(int row, int[] index)
        {
            int maxIndex = row;
			double max = AMatrix[row, index[maxIndex]];
            double maxAbs = Math.Abs(max);

			for (int i = row + 1; i < Size; ++i)
            {
				double cur = AMatrix[row, index[i]];

				if (Math.Abs(cur) > maxAbs)
                {
					maxIndex = i;
                    max = cur;
					maxAbs = Math.Abs(cur);
                }
            }
				
            int temp = index[row];
			index[row] = index[maxIndex];
			index[maxIndex] = temp;

            return max;
        }
    }
}
