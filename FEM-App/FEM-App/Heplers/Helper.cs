﻿using FEM_App.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FEM_App
{
    static class Helper
    {
        public static List<ResultVectorWithPoint> GetResultOnBounde(double[] Xvector, List<List<float>> CT)
        {
            List<ResultVectorWithPoint> res = new List<ResultVectorWithPoint>();
            for (int i = 0; i < CT.Count; i++)
            {
                if (CT[i][1] == 0f)//всі y = 0
                {
                    res.Add(new ResultVectorWithPoint() { X = CT[i][0], Y = CT[i][1], result = Xvector[i] });
                }
            }
            return res;
        }

        public static List<ResultVectorWithPoint> GetResultOnLine(double[] Xvector, List<List<float>> CT,double LinePoint)
        {
            List<ResultVectorWithPoint> res = new List<ResultVectorWithPoint>();
            for (int i = 0; i < CT.Count; i++)
            {
                if (CT[i][1] == LinePoint)//всі y = 0.5
                {
                    res.Add(new ResultVectorWithPoint() { X = CT[i][0], Y = CT[i][1], result = Xvector[i] });
                }
            }
            return res;
        }

        public static List<List<float>> GetPointInLine(List<List<float>> CT,double Line)
        {
            List<List<float>> res = new List<List<float>>();
            for (int i = 0; i < CT.Count; i++)
            {
                if (CT[i][1] == Line)
                {
                    res.Add(new List<float>() { CT[i][0], CT[i][1] });
                }
            }
            return res;
        }

        public static List<double> GetDerivToNormal(List<ResultVectorWithPoint> _boundaryPoint)
        {
            float step = getMeshStep(_boundaryPoint[1].X, _boundaryPoint[0].X);

            _boundaryPoint.Sort(delegate (ResultVectorWithPoint firstObj, ResultVectorWithPoint secondObj)
            {
                return firstObj.Y.CompareTo(secondObj.Y);
            });
            
            List<double> res = new List<double>();
            for (int i = 0; i < _boundaryPoint.Count - 1; i++)
            {
                double temp = _boundaryPoint[i + 1].result - _boundaryPoint[i].result;
                res.Add(temp / step);
            }

            return res;
        }

        private static float getMeshStep(float first, float second)
        {
            return second - first;
        }

        public static double GetErrorAbs(List<ResultVectorWithPoint> currentPoint, List<ResultVectorWithPoint> exactPoint)
        {
            currentPoint.Sort(delegate (ResultVectorWithPoint firstObj, ResultVectorWithPoint secondObj)
            {
                return firstObj.X.CompareTo(secondObj.X);
            });
            exactPoint.Sort(delegate (ResultVectorWithPoint firstObj, ResultVectorWithPoint secondObj)
            {
                return firstObj.X.CompareTo(secondObj.X);
            });

            List<double> error = new List<double>();
            for(int i = 0; i < currentPoint.Count; i++)
            {
                error.Add(Math.Abs((double)(currentPoint[i].result - exactPoint[i].result)));
            }

            return error.Max();
        }
        public static double GetErrorRel(List<ResultVectorWithPoint> currentPoint, List<ResultVectorWithPoint> exactPoint)
        {
            currentPoint.Sort(delegate (ResultVectorWithPoint firstObj, ResultVectorWithPoint secondObj)
            {
                return firstObj.X.CompareTo(secondObj.X);
            });
            exactPoint.Sort(delegate (ResultVectorWithPoint firstObj, ResultVectorWithPoint secondObj)
            {
                return firstObj.X.CompareTo(secondObj.X);
            });

            List<double> error = new List<double>();

            for (int i = 0; i < currentPoint.Count; i++)
            {
                error.Add(Math.Abs((double)(exactPoint[i].result - currentPoint[i].result)));
            }

            List<double> Current = new List<double>();

            for (int i = 0; i < currentPoint.Count; i++)
            {
                Current.Add(currentPoint[i].result);
            }

            double znam = Norma(error);
            double chis = Norma(Current);
            return znam / chis*100;
        }

        public static double Norma(List<double> pointList){
            double sum = 0;
            for (int i = 0; i < pointList.Count; i++)
            {
                sum += Math.Pow(pointList[i],2);
            }

            return Math.Sqrt(sum);
        }

        public static List<PointF> ToPointFListCT(List<List<float>> CT)
        {
            List<PointF> res = new List<PointF>();
            for (int i = 0; i < CT.Count; i++)
            {
                res.Add(new PointF((float)CT[i][0], (float)CT[i][1]));
            }
            return res;
        }

        public static List<List<PointF>> ToTriangleList_NT(List<List<float>> CT, List<List<int>> NT)
        {
            var res = new List<List<PointF>>();
            for (int i = 0; i < NT.Count; i++)
            {
                res.Add(new List<PointF>()
                {
                    new PointF(CT[NT[i][0]][0],CT[NT[i][0]][1]),
                    new PointF(CT[NT[i][1]][0],CT[NT[i][1]][1]),
                    new PointF(CT[NT[i][2]][0],CT[NT[i][2]][1]),
                });
            }

            return res;
        }

        public static PointF ToPointF(List<float> CT)
        {
            return new PointF(CT[0],CT[1]);
        }
    }
}
