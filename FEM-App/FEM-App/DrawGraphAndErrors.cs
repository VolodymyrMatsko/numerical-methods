﻿using FEM_App.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FEM_App
{
    public partial class DrawGraphAndErrors : Form
    {
        private List<ResultVectorWithPoint> CurrentResult { get; set; }
        private List<ResultVectorWithPoint> ExactResult { get; set; }
        public DrawGraphAndErrors(List<ResultVectorWithPoint> _currentResult, List<ResultVectorWithPoint> _exactResult)
        {
            InitializeComponent();
            CurrentResult = _currentResult;
            ExactResult = _exactResult;

            Sort();

            Draw();

            WriteErrors();
        }
        private void Sort()
        {
            CurrentResult.Sort(delegate (ResultVectorWithPoint firstObj, ResultVectorWithPoint secondObj)
            {
                return firstObj.X.CompareTo(secondObj.X);
            });
            ExactResult.Sort(delegate (ResultVectorWithPoint firstObj, ResultVectorWithPoint secondObj)
            {
                return firstObj.X.CompareTo(secondObj.X);
            });
        }
        private void Draw()
        {
            chart1.Series[0].LegendText = "Current";
            chart1.Series[1].LegendText = "Exact";
            for (int i = 0; i < CurrentResult.Count; i++)
            {
                chart1.Series[0].Points.AddXY(CurrentResult[i].X, CurrentResult[i].result);
                chart1.Series[1].Points.AddXY(ExactResult[i].X, ExactResult[i].result);
            }
        }
        private void WriteErrors()
        {
            
        }

    }
}
