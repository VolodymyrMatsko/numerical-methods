﻿using FEM_App.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FEM_App.Services
{
    public class ExactSolutionServices
    {
        private EquationForExact equationForExact { get; set; }
        private BoundaryForExact boundaryForExact { get; set; }
        private List<double> interval { get; set; }
        private List<double> constants { get; set; }
        
        public ExactSolutionServices(Equation _equation,BoundaryCondition _boundaryCondition, List<double> _interval)
        {
            equationForExact = new EquationForExact() { A11 = _equation.A11, D = _equation.D, F = _equation.F };
            boundaryForExact = new BoundaryForExact() { U_A = _boundaryCondition.Uc[3][0], U_B = _boundaryCondition.Uc[1][0] };
            interval = _interval;
            SolveConst();
            //GetRes();
        }
        public List<ResultVectorWithPoint> GetSolutionToPoint(List<List<float>> _point)
        {
            List<ResultVectorWithPoint> res = new List<ResultVectorWithPoint>();
            for(int i = 0; i < _point.Count; i++)
            {
                double Too = GetResult(0.5);
                res.Add(new ResultVectorWithPoint()
                {
                    X = _point[i][0],
                    Y = _point[i][1],
                    result = GetResult(_point[i][0])//знаходимо в X
                });
            }
            return res;
        }

        public void GetRes()
        {
            double u_a = 3;
            double u_b = 2.0;
            constants[1] = (u_b - (u_a - (-1)*equationForExact.F(0, 0, 0) / equationForExact.D) * Math.E - (-1) * equationForExact.F(0, 0, 0) / equationForExact.D)/
                (-Math.E+1.0/Math.E);

            constants[0] = u_a - (-1) * equationForExact.F(0, 0, 0) / equationForExact.D - constants[1];
        }

        private void SolveConst()
        {
            double[] bvector = new double[2] {
                boundaryForExact.U_A - (-1)*equationForExact.F(0, 0, 0) / equationForExact.D,
                //3.0 - equationForExact.F(0, 0, 0) / equationForExact.D,
                boundaryForExact.U_B - (-1)*equationForExact.F(0, 0, 0) / equationForExact.D };
                //2.0 - equationForExact.F(0, 0, 0) / equationForExact.D };
            double[,] matrix = new double[2, 2];

            for(int i = 0; i < 2; i++)
            {
                for(int j = 0; j < 2; j++)
                {
                    List<double> lambda = GetLambda();
                    matrix[i, j] = Math.Exp(GetLambda()[j]*interval[i]);
                }
            }
            GaussSolver gauss = new GaussSolver(matrix, bvector);
            gauss.GaussSolve();
            constants = gauss.XVector.ToList();
        }

        private double GetResult(double x)
        {
            if (equationForExact.D == 0)
            {
                return ((x * x) - x + 28.0)/14.0;
            }
            List<double> res = GetLambda();
            double x1 = Math.Exp(GetLambda()[0] * x);
            double x2 = Math.Exp(GetLambda()[1] * x);
            double firstdod = constants[0] * Math.Exp(GetLambda()[0] * x);
            double secondtdod = constants[1] * Math.Exp(GetLambda()[1] * x);
            double chastkaResult = (-1) * equationForExact.F(0, 0, 0) / equationForExact.D;
            return firstdod + secondtdod + chastkaResult;
        }

        private List<double> GetLambda()
        {
            return new List<double>() { Math.Sqrt(equationForExact.D / equationForExact.A11),
                -Math.Sqrt(equationForExact.D / equationForExact.A11) };
        }
    }
}
