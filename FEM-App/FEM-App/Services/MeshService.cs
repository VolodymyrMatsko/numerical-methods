﻿using FEM_App.Extensions;
using FEM_App.Store.Data;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TriangleNet;
using TriangleNet.Data;
using TriangleNet.Geometry;

namespace FEM_App.Services
{
    public class MeshService
    {
        public TriangleNet.Mesh mesh;
        public List<List<float>> CT { get; set; }
        public List<List<int>> NT { get; set; }
        public List<List<Edge>> NTG { get; set; }

        public List<Triangle> Triangle { get; set; }

        //private Polygon _area; // полігон
        private List<Edge> _borders = new List<Edge>(); //границі
        private MatrixService _matrixService;
        public InputGeometry _input { get; set; }
        public double _maxArea;
        public double _maxAngle;
        public double H;

        public MeshService(List<PointF> point, double maxArea){
            _input = new InputGeometry();
            _maxArea = maxArea;
            _maxAngle = 91;
            AddPointsToInput(point);
            CT = new List<List<float>>();
            NT = new List<List<int>>();
            NTG = new List<List<Edge>>();
            for (int i = 0; i < point.Count; i++)
            {
                NTG.Add(new List<Edge>());
            }
        }

        public void Triangulate(List<PointF> jointPoints = null){
            AddPointsToInput(jointPoints);
            Behavior b = new Behavior();
            b.ConformingDelaunay = true;
            b.Quality = true;
            b.Algorithm = TriangulationAlgorithm.SweepLine;
            b.MinAngle = 32;
            //var area = (Math.Sqrt(3.0) * H * H) / 4.0;
            //b.MaxArea = area + 0.5*area;
            b.MaxArea = _maxArea;
            //b.MaxArea = 0.5;
            mesh = new Mesh(b);
            mesh.Triangulate(_input);
            bool cons;
            bool del;
            mesh.Check(out cons, out del);
            
            SetCT(mesh.Vertices.ToPointFList());
            SetBorders(_input);
            SetNT(mesh.Triangles.ToList());
            SetNTG(mesh.Segments.ToList());
            Triangle = mesh.Triangles.ToList();
        }

        public void AddLoad(List<List<float>> point, List<Triangle> triangle)
        {
            var geometry = new InputGeometry();
            point.ForEach(x => { geometry.AddPoint(x[0], x[1]); });

            var tr = new List<ITriangle>();
            triangle.ForEach(x => { tr.Add(x); });

            mesh.Load(geometry, tr);
        }

        private void AddPointToLine(){
            //var points = GetTestPoints(Inputs.FixedLine.Start, Inputs.FixedLine.End);
            //foreach (var point in points)
            //{
            //    mesh.InsertVertex(new Vertex(point.X,point.Y), mesh);
            //}

            //double step = 1.0/countPoint;
            //for(int i = 0; i <= countPoint; i++)
            //{
            //    Vertex point = new Vertex(i * step, LinePoint);
            //    mesh.InsertVertex(point, mesh);
            //}
        }

        public List<double> GetElementsArea(List<Triangle> triangles){
            List<double> areas = new List<double>();
            areas.AddRange(triangles.Select(t => t.Area).ToList());
            return areas;
        }

        private void SetBorders(InputGeometry input){
            List<TriangleNet.Geometry.Point> points = input.Points.ToList();
            //_borders = listBounde;

            for (int i = 0; i < 3; i++)
            {
                _borders.Add(new Edge(FindNumberPoint(points[i].ToPointF()),
                    FindNumberPoint(points[i + 1].ToPointF())));
            }

            _borders.Add(new Edge(FindNumberPoint(points[3].ToPointF()), FindNumberPoint(points[0].ToPointF())));
        }

        private void AddPointsToInput(List<PointF> point){
            //List<Vertex> point_vertex = new List<Vertex>();
            if (point != null)
            {
                for (int i = 0; i < point.Count; i++)
                {
                    _input.AddPoint(point[i].X, point[i].Y);
                }
            }

            //List<PointF> LinePoint = GetTestPoints(GetCountPointDependMaxArea(_maxArea), Inputs.FixedLine.Start, Inputs.FixedLine.End);
            //for (int i = 0; i < LinePoint.Count; i++)
            //{
            //    _input.AddPoint(LinePoint[i].X, LinePoint[i].Y);
            //}

            //Contour cont = new Contour(point_vertex);
            //_area = new Polygon();
            //_area.Add(cont);
        }

        //private void AddCutPoints(List<PointF> points)
        //{
        //    AddPointsToInput(points);
        //}

        private void SetCT(List<PointF> vertices){
            for (int i = 0; i < vertices.Count; i++)
            {
                CT.Add(new List<float>() {vertices[i].X, vertices[i].Y});
            }
        }

        private void SetNT(List<Triangle> triangles){
            for (int ii = 0; ii < triangles.Count; ii++)
            {
                Triangle tr = new Triangle();
                tr = triangles[ii];
                NT.Add(new List<int>()
                {
                    tr.P0,
                    tr.P1,
                    tr.P2,
                });
            }
        }

        bool isClockwise(int i, int j, int m){
            return (CT[j][0] - CT[i][0]) * (CT[j][1] + CT[i][1]) +
                   (CT[m][0] - CT[j][0]) * (CT[m][1] + CT[j][1]) +
                   (CT[i][0] - CT[m][0]) * (CT[i][1] + CT[m][1]) > 0;
        }

        private void SetNTG(List<Segment> segments){
            for (int i = 0; i < NTG.Count; i++) //по границям
            {
                Tuple<double, string> SameValueBounde =
                    GetSameValue(GetPoint(_borders[i].P0), GetPoint(_borders[i].P1));
                for (int j = 0; j < segments.Count; j++) //по всім елементам на границі
                {
                    //щоб знайти відповідні граничні точки
                    Tuple<double, string> SameValueSegment =
                        GetSameValue(segments[j].GetVertex(1), segments[j].GetVertex(0));

                    if (SameValueBounde.Item1 == SameValueSegment.Item1 &&
                        SameValueBounde.Item2 == SameValueSegment.Item2)
                    {
                        NTG[i].Add(new Edge(segments[j].P0, segments[j].P1));
                    }
                }
            }
        }

        private Tuple<double, string> GetSameValue(Vertex vertexFirst, Vertex vertexSecond){
            Tuple<double, string> res = new Tuple<double, string>(0, "");
            if (vertexFirst.X == vertexSecond.X)
            {
                return new Tuple<double, string>(vertexSecond.X, "X");
            }
            else if (vertexFirst.Y == vertexSecond.Y)
            {
                return new Tuple<double, string>(vertexSecond.Y, "Y");
            }

            return null;
        }

        private int FindNumberPoint(PointF point){
            for (int i = 0; i < CT.Count; i++)
            {
                if (CT[i][0] == point.X && CT[i][1] == point.Y)
                {
                    return i;
                }
            }

            return -1;
        }

        private Vertex GetPoint(int number){
            return new Vertex(CT[number][0], CT[number][1]);
        }

        private List<PointF> GetTestPoints(int countPoint, PointF start, PointF end){
            //float step = step;
            float step = (end.X - start.X) / countPoint;
            H = step;
            List<PointF> res = new List<PointF>();
            PointF currentPoint = start;
            if (start.X == end.X)
            {
                while (currentPoint.Y <= end.Y)
                {
                    res.Add(new PointF(currentPoint.X, currentPoint.Y));
                    currentPoint.Y += step;
                }

                res.Add(end);
            }
            else
            {
                while (currentPoint.X <= end.X)
                {
                    res.Add(new PointF(currentPoint.X, currentPoint.Y));
                    currentPoint.X += step;
                }
            }

            return res;
        }

        public int GetCountPointDependMaxArea(double maxArea){
            if (maxArea > 0.9)
            {
                return 2;
            }
            else if (maxArea > 0.5)
            {
                return 3;
            }
            else if (maxArea > 0.2)
            {
                return 4;
            }
            else if (maxArea > 0.1)
            {
                return 5;
            }
            else
            {
                return 13;
            }
        }

        private bool IsCorrectTriangle(List<PointF> points){
            PointF firstPoint = points[0];
            PointF secondPoint = points[1];
            PointF thirdPoint = points[2];

            List<double> cosList = new List<double>();

            double distanseA = GetDistance(firstPoint, secondPoint);
            double distanseB = GetDistance(secondPoint, thirdPoint);
            double distanseC = GetDistance(thirdPoint, firstPoint);

            cosList.Add((Math.Pow(distanseB, 2) + Math.Pow(distanseC, 2) - Math.Pow(distanseA, 2)) /
                        (2 * distanseC * distanseB));
            cosList.Add((Math.Pow(distanseA, 2) + Math.Pow(distanseC, 2) - Math.Pow(distanseB, 2)) /
                        (2 * distanseA * distanseC));
            cosList.Add((Math.Pow(distanseA, 2) + Math.Pow(distanseB, 2) - Math.Pow(distanseC, 2)) /
                        (2 * distanseA * distanseB));
            foreach (double cos in cosList)
            {
                double tempAngle = (Math.Acos(cos) * 180 / Math.PI);
                if (_maxAngle < tempAngle)
                {
                    return false;
                }
            }

            return true;
        }

        private double GetDistance(PointF firstPointF, PointF secondPointF){
            double firstConjunction = Math.Pow(secondPointF.X - firstPointF.X, 2);
            double secondConjunction = Math.Pow(secondPointF.Y - firstPointF.Y, 2);
            return Math.Sqrt(firstConjunction + secondConjunction);
        }

        private List<PointF> GetTrianglePoint(Triangle triangle, List<PointF> vertices){
            return new List<PointF>() {vertices[triangle.P0], vertices[triangle.P1], vertices[triangle.P2]};
        }
    }
}