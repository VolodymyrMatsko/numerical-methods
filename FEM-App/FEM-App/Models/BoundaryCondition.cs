﻿using System;
using System.Collections.Generic;

namespace FEM_App
{
	public class BoundaryCondition
	{
		public double[] Beta { get; set; }
		public double[] Sigma { get; set; }

		public List<List<double>> Uc { get; set; }
	}
}

