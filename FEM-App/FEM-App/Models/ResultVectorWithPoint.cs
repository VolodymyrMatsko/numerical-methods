﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FEM_App.Models
{
    public class ResultVectorWithPoint
    {
        public float X { get; set; }
        public float Y { get; set; }
        public double result { get; set; }
    }
}
