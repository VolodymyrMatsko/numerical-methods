﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TriangleNet.Data;

namespace FEM_App.Heplers
{
    class PointHelper
    {
        private const double eps = 0.001;

        public static List<List<PointF>> GetTriangleWithBelongPoint(List<PointF> points, List<List<PointF>> triangles)
        {
            var res = new List<List<PointF>>();
            for (int i = 0; i < points.Count; i++)
            {
                for (int j = 0; j < triangles.Count; j++)
                {
                    if (IsPointInArea(points[i], triangles[j]))
                    {
                        res.Add(triangles[j]);
                        break;
                    }
                }
            }
            return res;
        }

        public static List<PointF> GetPointInArea(List<PointF> points, List<PointF> area)
        {
            var res = new List<PointF>();
            for (int i = 0; i < points.Count; i++)
            {
                if (IsPointInArea(points[i], area))
                {
                    res.Add(points[i]);
                }
            }

            return res;
        }

        public static bool IsPointInArea(PointF point, List<PointF> area)
        {
            double S = 0;
            for (int i = 0; i < area.Count-1; i++)
            {
                double tempS = GetTriangleSquare(area[i], area[i + 1], point);
                S += tempS;
            }
            S+= GetTriangleSquare(area[area.Count-1], area[0], point);

            double areaSquare = 0;
            if (area.Count == 3)
            {
                areaSquare = GetTriangleSquare(area[0], area[1], area[2]);
            }
            else if(area.Count==4)
            {
                areaSquare = GetVectorLength(area[0], area[1]) * GetVectorLength(area[1], area[2]);
            }

            if (Math.Abs(S - areaSquare) < eps)
            {
                return true;
            }
            return false;
        }

        private static double GetVectorLength(PointF point1, PointF point2)
        {
            return Math.Sqrt(Math.Pow(point1.X - point2.X, 2) + Math.Pow(point1.Y - point2.Y, 2));
        }

        private static double GetTriangleSquare(PointF point1, PointF point2, PointF point3)
        {
            return Math.Abs(0.5 * ((point1.X * point2.Y + point2.X * point3.Y + point3.X * point1.Y) -
                          (point1.Y * point2.X + point2.Y * point3.X + point3.Y * point1.X)));
        }
    }
}
