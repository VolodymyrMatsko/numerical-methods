$('#reload-btn').click(() => {
  $.getJSON('/result', (data) => {
    var data = JSON.parse(data);
    drawVisualization(data);
  })
})

function drawVisualization(dataset) {
  var data = new vis.DataSet();
  var ct = dataset.CT.map((item) => {return {x: item[0], y: item[1]}});
  var res = dataset.Result;

  for(var i = 0; i < ct.length; i++) {
    data.add({
      x: ct[i].x,
      y: ct[i].y,
      z: res[i],
      style: res[i]
    })
  }

  // specify options
  var options = {
    width:  '900px',
    height: '800px',
    style: 'bar',
    verticalRatio: 1.5
  };

  // create a graph3d
  var container = document.getElementById('mygraph');
  graph3d = new vis.Graph3d(container, data, options);
}